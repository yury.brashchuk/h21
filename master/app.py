
from flask import Flask
import requests

app = Flask(__name__)

@app.route("/")
def home():
    s = requests.get('http://slave_1:5000/').text
    return f"1 > {s}"

if __name__ == "__main__":
    app.run(host="0.0.0.0") 